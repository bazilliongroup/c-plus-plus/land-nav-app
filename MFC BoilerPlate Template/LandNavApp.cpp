#include "LanNavApp.h"
#include "LandNavigation.h"

LandNavApp::LandNavApp() {  }

BOOL LandNavApp::InitInstance()
{
	CWinApp::InitInstance();				// Call the Parent class instance
	LandNavigation dlg;							// Building an instance on the stack
	m_pMainWnd = &dlg;						// m_pMainWnd is Microsoft's Wizard code
	INT_PTR nResponse = dlg.DoModal();		// DoModal pops up your resource.
	return FALSE;

} //close function