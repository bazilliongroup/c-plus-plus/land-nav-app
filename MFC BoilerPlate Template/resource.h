//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by INTERFACE1.rc
//
#define INTERFACE1                      101
#define CB_START                        1001
#define CB_HIDE_SHOW                    1002
#define CB_NORTH_WEST                   1007
#define CB_NORTH                        1008
#define CB_NORTH_EAST                   1009
#define CB_WEST                         1010
#define CB_SOUTH_WEST                   1011
#define CB_SOUTH                        1012
#define CB_SOUTH_EAST                   1013
#define CB_EAST                         1014
#define EC_MGRS                         1015
#define PC_JPEG                         1016
#define IDC_BUTTON1                     1017
#define CB_RESET                        1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
