#include "LandNavigation.h"

#define HIDE_TEXT L"Hide MGRS"
#define SHOW_TEXT L"Show MGRS"
#define NO_MGRS -1

LandNavigation::LandNavigation(CWnd* pParent): CDialog(LandNavigation::IDD, pParent) 
{
	//Dimentions of Map
	// x1,y1,x2,y2 as (width = x2-x1), (height = y2-y1)
	m_x1 = 150;
	m_y1 = 35;
	m_x2 = 300;
	m_y2 = 285;

	m_MGRS_CurrentCoordinate = 130860;
	m_nextLocation = CURRENT;

	m_imageDirectory = L"Images/";
	m_myJpeg = L"tenino.jpg";

	InitilaizeMap();
}

BOOL LandNavigation::OnInitDialog() 
{ 
        CDialog::OnInitDialog();

		InitDialogItems();
		SetUpFontsAndColor();

		m_pHIDE_TOGGLE->ShowWindow( SW_HIDE );
		m_pNW->ShowWindow( SW_HIDE );
		m_pNORTH->ShowWindow( SW_HIDE );
		m_pNE->ShowWindow( SW_HIDE );
		m_pWEST->ShowWindow( SW_HIDE );
		m_pEAST->ShowWindow( SW_HIDE );
		m_pSW->ShowWindow( SW_HIDE );
		m_pSOUTH->ShowWindow( SW_HIDE );
		m_pSE->ShowWindow( SW_HIDE );
		m_pMGRS_Coordinate->ShowWindow( SW_HIDE );
		m_pReset->ShowWindow( SW_HIDE );

        return true; 
}

void LandNavigation::InitDialogItems()
{
		//Initilaize all Resource items in INITIALIZE.RC, Everything works via pointers.
		m_pSTART = (CButton *)GetDlgItem( CB_START );
		m_pHIDE_TOGGLE = (CButton *)GetDlgItem( CB_HIDE_SHOW );
		m_pNW = (CButton *)GetDlgItem( CB_NORTH_WEST );
		m_pNORTH = (CButton *)GetDlgItem( CB_NORTH );
		m_pNE = (CButton *)GetDlgItem( CB_NORTH_EAST );
		m_pSW = (CButton *)GetDlgItem( CB_SOUTH_WEST );
		m_pSOUTH = (CButton *)GetDlgItem( CB_SOUTH );
		m_pSE = (CButton *)GetDlgItem( CB_SOUTH_EAST );
		m_pWEST = (CButton *)GetDlgItem( CB_WEST );
		m_pEAST = (CButton *)GetDlgItem( CB_EAST );
		m_pMGRS_Coordinate = (CStatic *)GetDlgItem( EC_MGRS );
		m_pJpeg = (CStatic *)GetDlgItem( PC_JPEG );
		m_pReset = (CButton *)GetDlgItem( CB_RESET );
}

void LandNavigation::RepaintVectorMap()
{
	//Arguments are: Left, Top, Right, Bottom
	CRect CLEARBOX(m_x1, m_y1, m_x2, m_y2);
	GetClientRect( &CLEARBOX );
	CLEARBOX.MoveToX(m_x1);
	CLEARBOX.MoveToY(m_y1);
	InvalidateRect(CLEARBOX, 1);
	//RedrawWindow(CLEARBOX, NULL, NULL, RDW_INVALIDATE);	//Updates the specified rectangle or region in the given window's client area.
}


void LandNavigation::STARTBUTTON()
{
	m_pSTART->EnableWindow( false );		//Disables the Start Button
	m_pNW->ShowWindow( SW_SHOW );
	m_pNORTH->ShowWindow( SW_SHOW );
	m_pNE->ShowWindow( SW_SHOW );
	m_pWEST->ShowWindow( SW_SHOW );
	m_pEAST->ShowWindow( SW_SHOW );
	m_pSW->ShowWindow( SW_SHOW );
	m_pSOUTH->ShowWindow( SW_SHOW );
	m_pSE->ShowWindow( SW_SHOW );
	m_pMGRS_Coordinate->ShowWindow( SW_SHOW );
	m_pReset->ShowWindow( SW_SHOW );

	m_MGRS_Show = SHOW;
	m_pHIDE_TOGGLE->ShowWindow( SW_SHOW );
	m_pHIDE_TOGGLE->SetWindowTextW( HIDE_TEXT );
}

void LandNavigation::DIR_NORTHWEST()
{
	m_nextLocation = NORTHWEST;
	RepaintVectorMap();
}

void LandNavigation::DIR_NORTHEAST()
{
	m_nextLocation = NORTHEAST;
	RepaintVectorMap();
}

void LandNavigation::DIR_NORTH()
{
	m_nextLocation = NORTH;
	RepaintVectorMap();
}

void LandNavigation::DIR_WEST()
{
	m_nextLocation = WEST;
	RepaintVectorMap();
}

void LandNavigation::DIR_EAST()
{
	m_nextLocation = EAST;
	RepaintVectorMap();
}

void LandNavigation::DIR_SOUTHWEST()
{
	m_nextLocation = SOUTHWEST;
	RepaintVectorMap();
}

void LandNavigation::DIR_SOUTH()
{
	m_nextLocation = SOUTH;
	RepaintVectorMap();
}

void LandNavigation::DIR_SOUTHEAST()
{
	m_nextLocation = SOUTHEAST;
	RepaintVectorMap();
}

void LandNavigation::RESET()

{
	m_MGRS_CurrentCoordinate = 130860;
	m_nextLocation = CURRENT;
	RepaintVectorMap();
}

void LandNavigation::HIDESHOW()
{
	CString mgrsText;

	switch(m_MGRS_Show)
	{
		case SHOW:
			mgrsText.Format(L"-------");
			m_pHIDE_TOGGLE->SetWindowTextW( SHOW_TEXT );
			m_MGRS_Show = HIDE;
			break;
		case HIDE:
			mgrsText.Format(L"%d", m_MGRS_CurrentCoordinate);
			m_pHIDE_TOGGLE->SetWindowTextW( HIDE_TEXT );
			m_MGRS_Show = SHOW;
			break;
	}

	m_pMGRS_Coordinate->SetWindowTextW(mgrsText);

}

//Sets color in dialog
//pDC - pointer to device context
//pWnd - pointer to control asking for color information
//nCtlColor - symbolic constant for specifying MFC component
HBRUSH LandNavigation::OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor )
{
	switch (nCtlColor)
	{
		case CTLCOLOR_BTN:
		case CTLCOLOR_LISTBOX:
		case CTLCOLOR_MSGBOX:
		case CTLCOLOR_SCROLLBAR:
		case CTLCOLOR_EDIT:
		case CTLCOLOR_STATIC:
			pDC->SetTextColor(RGB(200,255,0));
			pDC->SetBkColor(RGB(0,0,0));		//Sets the text background color. Like a highlighter.
		case CTLCOLOR_DLG: 
			return m_Brush_Main;
		default: 
			return OnCtlColor(pDC, pWnd, nCtlColor); 
	}
}

void LandNavigation::SetUpFontsAndColor()
{
	m_Brush_Main.CreateSolidBrush(RGB(0,0,0));
}

void LandNavigation::OnPaint()
{
	RepaintVectorMap();
	CPaintDC dc( this );	//Create a Paint Object and put the current class as its object 
	DrawMap(dc);			//Draw using the Paint Object as its argument.
}

void LandNavigation::DrawMap( CPaintDC & dc )
{
	//Display the JPEG image
	DisplayJpegImage();

	//Draw Player on MGRS Map
	PlatoonTracking( dc );
}

void LandNavigation::DisplayJpegImage()
{
	CImage ViewImage;
	CImage ViewBitMap;

	ViewImage.Load( m_imageDirectory + m_myJpeg );
	ViewBitMap.Attach( ViewImage.Detach() );
	m_pJpeg->SetBitmap( (HBITMAP)ViewBitMap );
}

void LandNavigation::PlatoonTracking( CPaintDC & dc )
{
	//Create Drawing Objects
	CBrush* myBrush = new CBrush();

	myBrush->CreateSolidBrush( RGB(25, 0, 100) );
	dc.SelectObject( myBrush );
	CPen myPen( PS_SOLID, 2, RGB(0, 0, 0) );

	MGRS_PIXELS mgrsPixels;

	//This will return the Pixel Coodinates based on the MGRS coordinate and direction selected. 
	Find_MGRS( m_MGRS_CurrentCoordinate , m_nextLocation, mgrsPixels );

	CString mgrsText;
	if (m_MGRS_Show == SHOW)
	{
		mgrsText.Format(L"%d", m_MGRS_CurrentCoordinate);
		m_pMGRS_Coordinate->SetWindowTextW(mgrsText);
	}
	dc.Ellipse(mgrsPixels.m_upX, mgrsPixels.m_upY, mgrsPixels.m_lowX, mgrsPixels.m_lowY);
}

bool LandNavigation::Find_MGRS( int mgrs, NextLocation location, MGRS_PIXELS &pixelCoord )
{
	mgrs = GetNextCoordinate( mgrs, location );
	MGRS_COORDINATES_POSITIONS mgrsCoordPos = GetCoordinate( mgrs );
	pixelCoord = mgrsCoordPos.pixelCoord;
	m_MGRS_CurrentCoordinate = mgrs;

	return true;
}

void LandNavigation::SetCoordinate(int nw, int north, int ne, int west, int current, int east, int sw, int south, int se, int upX, int upY)
{
	MGRS_COORDINATES_POSITIONS coord;
	coord.northWest = nw;
	coord.north = north;
	coord.northEast = ne;
	coord.west = west;
	coord.current = current;
	coord.east = east;
	coord.southWest = sw;
	coord.south = south;
	coord.southEast = se;

	coord.pixelCoord.m_upX = upX;
	coord.pixelCoord.m_upY = upY;
	coord.pixelCoord.m_lowX = upX + 10;
	coord.pixelCoord.m_lowY = upY + 10;

	//Add the cord to CMap()
	m_PlayerPostions.SetAt( coord.current, coord );
}

LandNavigation::MGRS_COORDINATES_POSITIONS& LandNavigation::GetCoordinate( int mgrs )
{
	MGRS_COORDINATES_POSITIONS rValue;
	m_PlayerPostions.Lookup( mgrs, rValue );
	return rValue;
}

int LandNavigation::GetNextCoordinate( int mgrs, NextLocation location)
{
	int ret = NO_MGRS;

	MGRS_COORDINATES_POSITIONS rValue = GetCoordinate( mgrs );

	switch(location)
	{
		case NORTHWEST:
			ret = rValue.northWest;
			break;
		case NORTH:
			ret = rValue.north;
			break;
		case NORTHEAST:
			ret = rValue.northEast;
			break;
		case WEST:
			ret = rValue.west;
			break;
		case CURRENT:
			ret = rValue.current;
			break;
		case EAST:
			ret = rValue.east;
			break;
		case SOUTHWEST:
			ret = rValue.southWest;
			break;
		case SOUTH:
			ret = rValue.south;
			break;
		case SOUTHEAST:
			ret = rValue.southEast;
			break;
	}

	if (ret == NO_MGRS)
	{
		ret = mgrs; 
	}

	return ret;
}

void LandNavigation::InitilaizeMap()
{
	//             NW     NORTH    NE      WEST     CUR     EAST     SW     SOUTH    SE
	//----------------------------------------------------------------------------------------------------------
	//	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS , 0, 0 );

//-------------------------------------- row 86 col 13 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------

	// row 86 col 13 grid line 0
	SetCoordinate( NO_MGRS, 130862,  132862,  NO_MGRS, 130860,  132860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8613xP0, G8613yP0 ); // 130860
	SetCoordinate( 130862,  132862,  134862,  130860,  132860,  134860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8613xP2, G8613yP0 ); // 132860
	SetCoordinate( 132862,  134862,  136862,  132860,  134860,  136860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8613xP4, G8613yP0 ); // 134860
	SetCoordinate( 134862,  136862,  138862,  134860,  136860,  138860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8613xP6, G8613yP0 ); // 136860
	SetCoordinate( 136862,  138862,  140862,  136860,  138860,  140860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8613xP8, G8613yP0 ); // 138860

	// row 86 col 13 grid line 1
	SetCoordinate( NO_MGRS, 130864,  132864, NO_MGRS,  130862,  132862, NO_MGRS,  130860,  132860, G8613xP0, G8613yP2 ); // 130862
	SetCoordinate( 130864,  132864,  134864, 130862,   132862,  134862, 130860,   132860,  134860, G8613xP2, G8613yP2 ); // 132862
	SetCoordinate( 132864,  134864,  136864, 132862,   134862,  136862, 132860,   134860,  136860, G8613xP4, G8613yP2 ); // 134862
	SetCoordinate( 134864,  136864,  138864, 134862,   136862,  138862, 134860,   136860,  138860, G8613xP6, G8613yP2 ); // 136862
	SetCoordinate( 136864,  138864,  140864, 136862,   138862,  140862, 136860,   138860,  140860, G8613xP8, G8613yP2 ); // 138862

	// row 86 col 13 grid line 2
	SetCoordinate( NO_MGRS, 130866, 132866, NO_MGRS,   130864,  132864, NO_MGRS,  130862,  132862, G8613xP0, G8613yP4 ); // 130864
	SetCoordinate( 130866, 132866,  134866,  130864,   132864,  134864, 130862,   132862,  134862, G8613xP2, G8613yP4 ); // 132864
	SetCoordinate( 132866, 134866,  136866,  132864,   134864,  136864, 132862,   134862,  136862, G8613xP4, G8613yP4 ); // 134864
	SetCoordinate( 134866, 136866,  138866,  134864,   136864,  138864, 134862,   136862,  138862, G8613xP6, G8613yP4 ); // 136864
	SetCoordinate( 136866, 138866,  140866,  136864,   138864,  140864, 136862,   138862,  140862, G8613xP8, G8613yP4 ); // 138864

	// row 86 col 13 grid line 3
	SetCoordinate( NO_MGRS, 130868,  132868, NO_MGRS,  130866,  132866, NO_MGRS,  130864,  132864, G8613xP0, G8613yP6 ); // 130866
	SetCoordinate( 130868,  132868,  134868, 130866,   132866,  134866, 130864,   132864,  134864, G8613xP2, G8613yP6 ); // 132866
	SetCoordinate( 132868,  134868,  136868, 132866,   134866,  136866, 132864,   134864,  136864, G8613xP4, G8613yP6 ); // 134866
	SetCoordinate( 134868,  136868,  138868, 134866,   136866,  138866, 134864,   136864,  138864, G8613xP6, G8613yP6 ); // 136866
	SetCoordinate( 136868,  138868,  140868, 136866,   138866,  140866, 136864,   138864,  140864, G8613xP8, G8613yP6 ); // 138866

	// row 86 col 13 grid line 4
	SetCoordinate( NO_MGRS, 130870,  132870, NO_MGRS,  130868,  132868, NO_MGRS,  130866,  132866, G8613xP0, G8613yP8 ); // 130868
	SetCoordinate( 130870,  132870,  134870, 130868,   132868,  134868, 130866,   132866,  134866, G8613xP2, G8613yP8 ); // 132868
	SetCoordinate( 132870,  134870,  136870, 132868,   134868,  136868, 132866,   134866,  136866, G8613xP4, G8613yP8 ); // 134868
	SetCoordinate( 134870,  136870,  138870, 134868,   136868,  138868, 134866,   136866,  138866, G8613xP6, G8613yP8 ); // 136868
	SetCoordinate( 136870,  138870,  140870, 136868,   138868,  140868, 136866,   138866,  140866, G8613xP8, G8613yP8 ); // 138868

//-------------------------------------- row 86 col 14 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------

	// row 86 col 14 grid line 0
	SetCoordinate( 138862,  140862,  142862,  138860,  140860,  142860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8614xP0, G8614yP0 ); // 140860
	SetCoordinate( 140862,  142862,  144862,  140860,  142860,  144860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8614xP2, G8614yP0 ); // 142860
	SetCoordinate( 142862,  144862,  146862,  142860,  144860,  146860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8614xP4, G8614yP0 ); // 144860
	SetCoordinate( 144862,  146862,  148862,  144860,  146860,  148860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8614xP6, G8614yP0 ); // 146860
	SetCoordinate( 146862,  148862,  150862,  146860,  148860,  150860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8614xP8, G8614yP0 ); // 148860

	// row 86 col 14 grid line 1
	SetCoordinate( 138864,  140864,  142864,  138862,  140862,  142862,  138860,  140860,  142860,  G8614xP0, G8614yP2 ); // 140862
	SetCoordinate( 140864,  142864,  144864,  140862,  142862,  144862,  140860,  142860,  144860,  G8614xP2, G8614yP2 ); // 142862
	SetCoordinate( 142864,  144864,  146864,  142862,  144862,  146862,  142860,  144860,  146860,  G8614xP4, G8614yP2 ); // 144862
	SetCoordinate( 144864,  146864,  148864,  144862,  146862,  148862,  144860,  146860,  148860,  G8614xP6, G8614yP2 ); // 146862
	SetCoordinate( 146864,  148864,  150860,  146862,  148862,  150862,  146860,  148860,  150860,  G8614xP8, G8614yP2 ); // 148862

	// row 86 col 14 grid line 2
	SetCoordinate( 138866,  140866,  142866,  138864,  140864,  142864,  138862,  140862,  142862,  G8614xP0, G8614yP4 ); // 140864
	SetCoordinate( 140866,  142866,  144866,  140864,  142864,  144864,  140862,  142862,  144862,  G8614xP2, G8614yP4 ); // 142864
	SetCoordinate( 142866,  144866,  146866,  142864,  144864,  146864,  142862,  144862,  146862,  G8614xP4, G8614yP4 ); // 144864
	SetCoordinate( 144866,  146866,  148866,  144864,  146864,  148864,  144862,  146862,  148862,  G8614xP6, G8614yP4 ); // 146864
	SetCoordinate( 146866,  148866,  150866,  146864,  148864,  150864,  146862,  148862,  150862,  G8614xP8, G8614yP4 ); // 148864

	// row 86 col 14 grid line 3
	SetCoordinate( 138868,  140868,  142868,  138866,  140866,  142866,  138864,  140864,  142864,  G8614xP0, G8614yP6 ); // 140866
	SetCoordinate( 140868,  142868,  144868,  140866,  142866,  144866,  140864,  142864,  144864,  G8614xP2, G8614yP6 ); // 142866
	SetCoordinate( 142868,  144868,  146868,  142866,  144866,  146866,  142864,  144864,  146864,  G8614xP4, G8614yP6 ); // 144866
	SetCoordinate( 144868,  146868,  148868,  144866,  146866,  148866,  144864,  146864,  148864,  G8614xP6, G8614yP6 ); // 146866
	SetCoordinate( 146868,  148868,  150868,  146866,  148866,  150866,  146864,  148864,  150864,  G8614xP8, G8614yP6 ); // 148866

	// row 86 col 14 grid line 4
	SetCoordinate( 138870,  140870,  142870,  138868,  140868,  142868,  138866,  140866,  142866,  G8614xP0, G8614yP8 ); // 140868
	SetCoordinate( 140870,  142870,  144870,  140868,  142868,  144868,  140866,  142866,  144866,  G8614xP2, G8614yP8 ); // 142868
	SetCoordinate( 142870,  144870,  146870,  142868,  144868,  146868,  142866,  144866,  146866,  G8614xP4, G8614yP8 ); // 144868
	SetCoordinate( 144870,  146870,  148870,  144868,  146868,  148868,  144866,  146866,  148866,  G8614xP6, G8614yP8 ); // 146868
	SetCoordinate( 146870,  148870,  150870,  146868,  148868,  150868,  146866,  148866,  150866,  G8614xP8, G8614yP8 ); // 148868

//-------------------------------------- row 86 col 15 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------

	// row 86 col 15 grid line 0
	SetCoordinate( 148862,  150862,  152862,  148860,  150860,  152860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8615xP0, G8615yP0 ); // 150860
	SetCoordinate( 150862,  152862,  154862,  150860,  152860,  154860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8615xP2, G8615yP0 ); // 152860
	SetCoordinate( 152862,  154862,  156862,  152860,  154860,  156860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8615xP4, G8615yP0 ); // 154860
	SetCoordinate( 154862,  156862,  158862,  154860,  156860,  158860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8615xP6, G8615yP0 ); // 156860
	SetCoordinate( 156862,  158862,  160862,  156860,  158860,  160860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8615xP8, G8615yP0 ); // 158860

	// row 86 col 15 grid line 1
	SetCoordinate( 148864,  150864,  152864,  148862,  150862,  152862,  148860,  150860,  152860,  G8615xP0, G8615yP2 ); // 150862
	SetCoordinate( 150864,  152864,  154864,  150862,  152862,  154862,  150860,  152860,  154860,  G8615xP2, G8615yP2 ); // 152862
	SetCoordinate( 152864,  154864,  156864,  152862,  154862,  156862,  152860,  154860,  156860,  G8615xP4, G8615yP2 ); // 154862
	SetCoordinate( 154864,  156864,  158864,  154862,  156862,  158862,  154860,  156860,  158860,  G8615xP6, G8615yP2 ); // 156862
	SetCoordinate( 156864,  158864,  160864,  156862,  158862,  160862,  156860,  158860,  160860,  G8615xP8, G8615yP2 ); // 158862

	// row 86 col 15 grid line 2
	SetCoordinate( 148866,  150866,  152866,  148864,  150864,  152864,  148862,  150862,  152862,  G8615xP0, G8615yP4 ); // 150864
	SetCoordinate( 150866,  152866,  154866,  150864,  152864,  154864,  150862,  152862,  154862,  G8615xP2, G8615yP4 ); // 152864
	SetCoordinate( 152866,  154866,  156866,  152864,  154864,  156864,  152862,  154862,  156862,  G8615xP4, G8615yP4 ); // 154864
	SetCoordinate( 154866,  156866,  158866,  154864,  156864,  158864,  154862,  156862,  158862,  G8615xP6, G8615yP4 ); // 156864
	SetCoordinate( 156866,  158866,  160866,  156864,  158864,  160864,  156862,  158862,  160862,  G8615xP8, G8615yP4 ); // 158864

	// row 86 col 15 grid line 3
	SetCoordinate( 148868,  150868,  152868,  148866,  150866,  152866,  148864,  150864,  152864,  G8615xP0, G8615yP6 ); // 150866
	SetCoordinate( 150868,  152868,  154868,  150866,  152866,  154866,  150864,  152864,  154864,  G8615xP2, G8615yP6 ); // 152866
	SetCoordinate( 152868,  154868,  156868,  152866,  154866,  156866,  152864,  154864,  156864,  G8615xP4, G8615yP6 ); // 154866
	SetCoordinate( 154868,  156868,  158868,  154866,  156866,  158866,  154864,  156864,  158864,  G8615xP6, G8615yP6 ); // 156866
	SetCoordinate( 156868,  158868,  160868,  156866,  158866,  160866,  156864,  158864,  160864,  G8615xP8, G8615yP6 ); // 158866

	// row 86 col 15 grid line 4
	SetCoordinate( 148870,  150870,  152870,  148868,  150868,  152868,  148866,  150866,  152866,  G8615xP0, G8615yP8 ); // 150868
	SetCoordinate( 150870,  152870,  154870,  150868,  152868,  154868,  150866,  152866,  154866,  G8615xP2, G8615yP8 ); // 152868
	SetCoordinate( 152870,  154870,  156870,  152868,  154868,  156868,  152866,  154866,  156866,  G8615xP4, G8615yP8 ); // 154868
	SetCoordinate( 154870,  156870,  158870,  154868,  156868,  158868,  154866,  156866,  158866,  G8615xP6, G8615yP8 ); // 156868
	SetCoordinate( 156870,  158870,  160870,  156868,  158868,  160868,  156866,  158866,  160866,  G8615xP8, G8615yP8 ); // 158868

//-------------------------------------- row 86 col 16 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------

	// row 86 col 16 grid line 0
	SetCoordinate( 158862,  160862,  162862,  158860,  160860,  162860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8616xP0, G8616yP0 ); // 160860
	SetCoordinate( 160862,  162862,  164862,  160860,  162860,  164860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8616xP2, G8616yP0 ); // 162860
	SetCoordinate( 162862,  164862,  166862,  162860,  164860,  166860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8616xP4, G8616yP0 ); // 164860
	SetCoordinate( 164862,  166862,  168862,  164860,  166860,  168860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8616xP6, G8616yP0 ); // 166860
	SetCoordinate( 166862,  168862,  170862,  166860,  168860,  170860,	 NO_MGRS, NO_MGRS, NO_MGRS, G8616xP8, G8616yP0 ); // 168860

	// row 86 col 16 grid line 1
	SetCoordinate( 158864,  160864,  162864,  158862,  160862,  162862,  158860,  160860,  162860,  G8616xP0, G8616yP2 ); // 160862
	SetCoordinate( 160864,  162864,  164864,  160862,  162862,  164862,  160860,  162860,  164860,  G8616xP2, G8616yP2 ); // 162862
	SetCoordinate( 162864,  164864,  166864,  162862,  164862,  166862,  162860,  164860,  166860,  G8616xP4, G8616yP2 ); // 164862
	SetCoordinate( 164864,  166864,  168864,  164862,  166862,  168862,  164860,  166860,  168860,  G8616xP6, G8616yP2 ); // 166862
	SetCoordinate( 166864,  168864,  170864,  166862,  168862,  170862,  166860,  168860,  170860,  G8616xP8, G8616yP2 ); // 168862

	// row 86 col 16 grid line 2
	SetCoordinate( 158866,  160866,  162866,  158864,  160864,  162864,  158862,  160862,  162862,  G8616xP0, G8616yP4 ); // 160864
	SetCoordinate( 160866,  162866,  164866,  160864,  162864,  164864,  160862,  162862,  164862,  G8616xP2, G8616yP4 ); // 162864
	SetCoordinate( 162866,  164866,  166866,  162864,  164864,  166864,  162862,  164862,  166862,  G8616xP4, G8616yP4 ); // 164864
	SetCoordinate( 164866,  166866,  168866,  164864,  166864,  168864,  164862,  166862,  168862,  G8616xP6, G8616yP4 ); // 166864
	SetCoordinate( 166866,  168866,  170866,  166864,  168864,  170864,  166862,  168862,  170862,  G8616xP8, G8616yP4 ); // 168864

	// row 86 col 16 grid line 3
	SetCoordinate( 158868,  160868,  162868,  158866,  160866,  162866,  158864,  160864,  162864,  G8616xP0, G8616yP6 ); // 160866
	SetCoordinate( 160868,  162868,  164868,  160866,  162866,  164866,  160864,  162864,  164864,  G8616xP2, G8616yP6 ); // 162866
	SetCoordinate( 162868,  164868,  166868,  162866,  164866,  166866,  162864,  164864,  166864,  G8616xP4, G8616yP6 ); // 164866
	SetCoordinate( 164868,  166868,  168868,  164866,  166866,  168866,  164864,  166864,  168864,  G8616xP6, G8616yP6 ); // 166866
	SetCoordinate( 166868,  168868,  170868,  166866,  168866,  170866,  166864,  168864,  170864,  G8616xP8, G8616yP6 ); // 168186

	// row 86 col 16 grid line 3
	SetCoordinate( 158870,  160870,  162870,  158868,  160868,  162868,  158866,  160866,  162866,  G8616xP0, G8616yP8 ); // 160868
	SetCoordinate( 160870,  162870,  164870,  160868,  162868,  164868,  160866,  162866,  164866,  G8616xP2, G8616yP8 ); // 162868
	SetCoordinate( 162870,  164870,  166870,  162868,  164868,  166868,  162866,  164866,  166866,  G8616xP4, G8616yP8 ); // 164868
	SetCoordinate( 164870,  166870,  168870,  164868,  166868,  168868,  164866,  166866,  168866,  G8616xP6, G8616yP8 ); // 166868
	SetCoordinate( 166870,  168870,  170870,  166868,  168868,  170868,  166866,  168866,  170866,  G8616xP8, G8616yP8 ); // 168868

//-------------------------------------- row 86 col 17 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------

	// row 86 col 17 grid line 0
	SetCoordinate( 168862,  170862,  NO_MGRS, 168860,  170860,  NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, G8617xP0+2, G8617yP0 ); // 170860
	// row 86 col 17 grid line 1
	SetCoordinate( 168864,  170864,  NO_MGRS, 168862,  170862,  NO_MGRS, 168860, 170860,  NO_MGRS,  G8617xP0+2, G8617yP2 ); // 170860
	// row 86 col 17 grid line 2
	SetCoordinate( 168866,  170866,  NO_MGRS, 168864,  170864,  NO_MGRS, 168862, 170862,  NO_MGRS,  G8617xP0+2, G8617yP4 ); // 170860
	// row 86 col 17 grid line 3
	SetCoordinate( 168868,  170868,  NO_MGRS, 168866,  170866,  NO_MGRS, 168864, 170864,  NO_MGRS,  G8617xP0+2, G8617yP6 ); // 170860
	// row 86 col 17 grid line 4
	SetCoordinate( 168870,  170870,  NO_MGRS, 168868,  170868,  NO_MGRS, 168866, 170866,  NO_MGRS,  G8617xP0+2, G8617yP8 ); // 170860

//-------------------------------------- row 87 col 13 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------

	// row 87 col 13 grid line 0
	SetCoordinate( NO_MGRS, 130872,  132872,  NO_MGRS, 130870,  132870,  NO_MGRS, 130868,  132868,  G8713xP0, G8713yP0 ); // 130870
	SetCoordinate( 130872,  132872,  134872,  130870,  132870,  134870,	 130868,  132868,  134868,  G8713xP2, G8713yP0 ); // 132870 
	SetCoordinate( 132872,  134872,  136872,  132870,  134870,  136870,	 132868,  134868,  136868,  G8713xP4, G8713yP0 ); // 134870
	SetCoordinate( 134872,  136872,  138872,  134870,  136870,  138870,	 134868,  136868,  138868,  G8713xP6, G8713yP0 ); // 136870
	SetCoordinate( 136872,  138872,  140872,  136870,  138870,  140870,	 136868,  138868,  140868,  G8713xP8, G8713yP0-2 ); // 138870

	// row 87 col 13 grid line 1
	SetCoordinate( NO_MGRS, 130874,  132874,  NO_MGRS, 130872,  132872,  NO_MGRS, 130870,  132870,  G8713xP0, G8713yP2 ); // 130872
	SetCoordinate( 130874,  132874,  134874,  130872,  132872,  134872,	 130870,  132870,  134870,  G8713xP2, G8713yP2 ); // 132872
	SetCoordinate( 132874,  134874,  136874,  132872,  134872,  136872,	 132870,  134870,  136870,  G8713xP4, G8713yP2 ); // 134872
	SetCoordinate( 134874,  136874,  138874,  134872,  136872,  138872,	 134870,  136870,  138870,  G8713xP6, G8713yP2 ); // 136872
	SetCoordinate( 136874,  138874,  140874,  136872,  138872,  140872,	 136870,  138870,  140870,  G8713xP8, G8713yP2 ); // 138872

	// row 87 col 13 grid line 2
	SetCoordinate( NO_MGRS, 130876,  132876,  NO_MGRS, 130874,  132874,  NO_MGRS, 130872,  132872,  G8713xP0, G8713yP4 ); // 130874
	SetCoordinate( 130876,  132876,  134876,  130874,  132874,  134874,	 130872,  132872,  134872,  G8713xP2, G8713yP4 ); // 132874
	SetCoordinate( 132876,  134876,  136876,  132874,  134874,  136874,	 132872,  134872,  136872,  G8713xP4, G8713yP4 ); // 134874
	SetCoordinate( 134876,  136876,  138876,  134874,  136874,  138874,	 134872,  136872,  138872,  G8713xP6, G8713yP4 ); // 136874
	SetCoordinate( 136876,  138876,  140876,  136874,  138874,  140874,	 136872,  138872,  140872,  G8713xP8, G8713yP4 ); // 138874

	// row 87 col 13 grid line 3
	SetCoordinate( NO_MGRS, 130878,  132878,  NO_MGRS, 130876,  132876,  NO_MGRS, 130874,  132874,  G8713xP0, G8713yP6 ); // 130876
	SetCoordinate( 130878,  132878,  134878,  130876,  132876,  134876,	 130874,  132874,  134874,  G8713xP2, G8713yP6 ); // 132876
	SetCoordinate( 132878,  134878,  136878,  132876,  134876,  136876,	 132874,  134874,  136874,  G8713xP4, G8713yP6 ); // 134876
	SetCoordinate( 134878,  136878,  138878,  134876,  136876,  138876,	 134874,  136874,  138874,  G8713xP6, G8713yP6 ); // 136876
	SetCoordinate( 136878,  138878,  140878,  136876,  138876,  140876,	 136874,  138874,  140874,  G8713xP8, G8713yP6 ); // 138876

	// row 87 col 13 grid line 4
	SetCoordinate( NO_MGRS, 130880,  132880,  NO_MGRS, 130878,  132878,  NO_MGRS, 130876,  132876,  G8713xP0, G8713yP8 ); // 130878
	SetCoordinate( 130880,  132880,  134880,  130878,  132878,  134878,	 130876,  132876,  134876,  G8713xP2, G8713yP8 ); // 132878
	SetCoordinate( 132880,  134880,  136880,  132878,  134878,  136878,	 132876,  134876,  136876,  G8713xP4, G8713yP8 ); // 134878
	SetCoordinate( 134880,  136880,  138880,  134878,  136878,  138878,	 134876,  136876,  138876,  G8713xP6, G8713yP8 ); // 136878
	SetCoordinate( 136880,  138880,  140880,  136878,  138878,  140878,	 136876,  138876,  140876,  G8713xP8, G8713yP8 ); // 138878

//-------------------------------------- row 87 col 14 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------

	// row 87 col 14 grid line 0
	SetCoordinate( 138872,  140872,  142872,  138870,  140870,  142870,  138868,  140868,  142868,  G8714xP0, G8714yP0 ); // 140870
	SetCoordinate( 140872,  142872,  144872,  140870,  142870,  144870,	 140868,  142868,  144868,  G8714xP2, G8714yP0 ); // 142870
	SetCoordinate( 142872,  144872,  146872,  142870,  144870,  146870,	 142868,  144868,  146868,  G8714xP4, G8714yP0 ); // 144870
	SetCoordinate( 144872,  146872,  148872,  144870,  146870,  148870,	 144868,  146868,  148868,  G8714xP6, G8714yP0 ); // 146870
	SetCoordinate( 146872,  148872,  150872,  146870,  148870,  150870,	 146868,  148868,  150868,  G8714xP8, G8714yP0 ); // 148870

	// row 87 col 14 grid line 1
	SetCoordinate( 138874,  140874,  142874,  138872,  140872,  142872,  138870,  140870,  142870,  G8714xP0, G8714yP2 ); // 140872
	SetCoordinate( 140874,  142874,  144874,  140872,  142872,  144872,	 140870,  142870,  144870,  G8714xP2, G8714yP2 ); // 142872
	SetCoordinate( 142874,  144874,  146874,  142872,  144872,  146872,	 142870,  144870,  146870,  G8714xP4, G8714yP2 ); // 144872
	SetCoordinate( 144874,  146874,  148874,  144872,  146872,  148872,	 144870,  146870,  148870,  G8714xP6, G8714yP2 ); // 146872
	SetCoordinate( 146874,  148874,  150874,  146872,  148872,  150872,	 146870,  148870,  150870,  G8714xP8, G8714yP2 ); // 148872

	// row 87 col 14 grid line 2
	SetCoordinate( 138876,  140876,  142876,  138874,  140874,  142874,  138872,  140872,  142872,  G8714xP0, G8714yP4 ); // 140874
	SetCoordinate( 140876,  142876,  144876,  140874,  142874,  144874,	 140872,  142872,  144872,  G8714xP2, G8714yP4 ); // 142874
	SetCoordinate( 142876,  144876,  146876,  142874,  144874,  146874,	 142872,  144872,  146872,  G8714xP4, G8714yP4 ); // 144874
	SetCoordinate( 144876,  146876,  148876,  144874,  146874,  148874,	 144872,  146872,  148872,  G8714xP6, G8714yP4 ); // 146874
	SetCoordinate( 146876,  148876,  150876,  146874,  148874,  150874,	 146872,  148872,  150872,  G8714xP8, G8714yP4 ); // 148874

	// row 87 col 14 grid line 3
	SetCoordinate( 138878,  140878,  142878,  138876,  140876,  142876,  138874,  140874,  142874,  G8714xP0, G8714yP6 ); // 140876
	SetCoordinate( 140878,  142878,  144878,  140876,  142876,  144876,	 140874,  142874,  144874,  G8714xP2, G8714yP6 ); // 142876
	SetCoordinate( 142878,  144878,  146878,  142876,  144876,  146876,	 142874,  144874,  146874,  G8714xP4, G8714yP6 ); // 144876
	SetCoordinate( 144878,  146878,  148878,  144876,  146876,  148876,	 144874,  146874,  148874,  G8714xP6, G8714yP6 ); // 146876
	SetCoordinate( 146878,  148878,  150878,  146876,  148876,  150876,	 146874,  148874,  150874,  G8714xP8, G8714yP6 ); // 148876

	// row 87 col 14 grid line 4
	SetCoordinate( 138880,  140880,  142880,  138878,  140878,  142878,  138876,  140876,  142876,  G8714xP0, G8714yP8 ); // 140878
	SetCoordinate( 140880,  142880,  144880,  140878,  142878,  144878,	 140876,  142876,  144876,  G8714xP2, G8714yP8 ); // 142878
	SetCoordinate( 142880,  144880,  146880,  142878,  144878,  146878,	 142876,  144876,  146876,  G8714xP4, G8714yP8 ); // 144878
	SetCoordinate( 144880,  146880,  148880,  144878,  146878,  148878,	 144876,  146876,  148876,  G8714xP6, G8714yP8 ); // 146878
	SetCoordinate( 146880,  148880,  150880,  146878,  148878,  150878,	 146876,  148876,  150876,  G8714xP8, G8714yP8 ); // 148878

//-------------------------------------- row 87 col 15 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------

	// row 87 col 15 grid line 0
	SetCoordinate( 148872,  150872,  152872,  148870,  150870,  152870,  148868,  150868,  152868,  G8715xP0, G8715yP0 ); // 150870
	SetCoordinate( 150872,  152872,  154872,  150870,  152870,  154870,	 150868,  152868,  154868,  G8715xP2, G8715yP0 ); // 152870
	SetCoordinate( 152872,  154872,  156872,  152870,  154870,  156870,	 152868,  154868,  156868,  G8715xP4, G8715yP0 ); // 154870
	SetCoordinate( 154872,  156872,  158872,  154870,  156870,  158870,	 154868,  156868,  158868,  G8715xP6, G8715yP0 ); // 156870
	SetCoordinate( 156872,  158872,  160872,  156870,  158870,  160870,	 156868,  158868,  160868,  G8715xP8, G8715yP0 ); // 158870

	// row 87 col 15 grid line 1
	SetCoordinate( 148874,  150874,  152874,  148872,  150872,  152872,  148870,  150870,  152870,  G8715xP0, G8715yP2 ); // 150872
	SetCoordinate( 150874,  152874,  154874,  150872,  152872,  154872,	 150870,  152870,  154870,  G8715xP2, G8715yP2 ); // 152872
	SetCoordinate( 152874,  154874,  156874,  152872,  154872,  156872,	 152870,  154870,  156870,  G8715xP4, G8715yP2 ); // 154872
	SetCoordinate( 154874,  156874,  158874,  154872,  156872,  158872,	 154870,  156870,  158870,  G8715xP6, G8715yP2 ); // 156872
	SetCoordinate( 156874,  158874,  160874,  156872,  158872,  160872,	 156870,  158870,  160870,  G8715xP8, G8715yP2 ); // 158872

	// row 87 col 15 grid line 2
	SetCoordinate( 148876,  150876,  152876,  148874,  150874,  152874,  148872,  150872,  152872,  G8715xP0, G8715yP4 ); // 150874
	SetCoordinate( 150876,  152876,  154876,  150874,  152874,  154874,	 150872,  152872,  154872,  G8715xP2, G8715yP4 ); // 152874
	SetCoordinate( 152876,  154876,  156876,  152874,  154874,  156874,	 152872,  154872,  156872,  G8715xP4, G8715yP4 ); // 154874
	SetCoordinate( 154876,  156876,  158876,  154874,  156874,  158874,	 154872,  156872,  158872,  G8715xP6, G8715yP4 ); // 156874
	SetCoordinate( 156876,  158876,  160876,  156874,  158874,  160874,	 156872,  158872,  160872,  G8715xP8, G8715yP4 ); // 158874

	// row 87 col 15 grid line 3
	SetCoordinate( 148878,  150878,  152878,  148876,  150876,  152876,  148874,  150874,  152874,  G8715xP0, G8715yP6 ); // 150876
	SetCoordinate( 150878,  152878,  154878,  150876,  152876,  154876,	 150874,  152874,  154874,  G8715xP2, G8715yP6 ); // 152876
	SetCoordinate( 152878,  154878,  156878,  152876,  154876,  156876,	 152874,  154874,  156874,  G8715xP4, G8715yP6 ); // 154876
	SetCoordinate( 154878,  156878,  158878,  154876,  156876,  158876,	 154874,  156874,  158874,  G8715xP6, G8715yP6 ); // 156876
	SetCoordinate( 156878,  158878,  160878,  156876,  158876,  160876,	 156874,  158874,  160874,  G8715xP8, G8715yP6 ); // 158876

	// row 87 col 15 grid line 4
	SetCoordinate( 148880,  150880,  152880,  148878,  150878,  152878,  148876,  150876,  152876,  G8715xP0, G8715yP8 ); // 150878
	SetCoordinate( 150880,  152880,  154880,  150878,  152878,  154878,	 150876,  152876,  154876,  G8715xP2, G8715yP8 ); // 152878
	SetCoordinate( 152880,  154880,  156880,  152878,  154878,  156878,	 152876,  154876,  156876,  G8715xP4, G8715yP8 ); // 154878
	SetCoordinate( 154880,  156880,  158880,  154878,  156878,  158878,	 154876,  156876,  158876,  G8715xP6, G8715yP8 ); // 156878
	SetCoordinate( 156880,  158880,  160880,  156878,  158878,  160878,	 156876,  158876,  160876,  G8715xP8, G8715yP8 ); // 158878

//-------------------------------------- row 87 col 16 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------

	// row 87 col 16 grid line 0
	SetCoordinate( 158872,  160872,  162872,  158870,  160870,  162870,  158868,  160868,  162868,  G8716xP0+2, G8716yP0 ); // 160870
	SetCoordinate( 160872,  162872,  164872,  160870,  162870,  164870,	 160868,  162868,  164868,  G8716xP2, G8716yP0 );   // 162870
	SetCoordinate( 162872,  164872,  166872,  162870,  164870,  166870,	 162868,  164868,  166868,  G8716xP4, G8716yP0-2 ); // 164870
	SetCoordinate( 164872,  166872,  168872,  164870,  166870,  168870,	 164868,  166868,  168868,  G8716xP6, G8716yP0-2 ); // 166870
	SetCoordinate( 166872,  168872,  170872,  166870,  168870,  170870,	 166868,  168868,  170868,  G8716xP8, G8716yP0-2 ); // 168870

	// row 87 col 16 grid line 1
	SetCoordinate( 158874,  160874,  162874,  158872,  160872,  162872,  158870,  160870,  162870,  G8716xP0+4, G8716yP2 ); // 160872
	SetCoordinate( 160874,  162874,  164874,  160872,  162872,  164872,	 160870,  162870,  164870,  G8716xP2, G8716yP2 );   // 162872
	SetCoordinate( 162874,  164874,  166874,  162872,  164872,  166872,	 162870,  164870,  166870,  G8716xP4, G8716yP2 );   // 164872
	SetCoordinate( 164874,  166874,  168874,  164872,  166872,  168872,	 164870,  166870,  168870,  G8716xP6, G8716yP2 );   // 166872
	SetCoordinate( 166874,  168874,  170874,  166872,  168872,  170872,	 166870,  168870,  170870,  G8716xP8, G8716yP2 );   // 168872

	// row 87 col 16 grid line 2
	SetCoordinate( 158876,  160876,  162876,  158874,  160874,  162874,  158872,  160872,  162872,  G8716xP0+4, G8716yP4 ); // 160874
	SetCoordinate( 160876,  162876,  164876,  160874,  162874,  164874,	 160872,  162872,  164872,  G8716xP2, G8716yP4 );   // 162874
	SetCoordinate( 162876,  164876,  166876,  162874,  164874,  166874,	 162872,  164872,  166872,  G8716xP4, G8716yP4 );   // 164874
	SetCoordinate( 164876,  166876,  168876,  164874,  166874,  168874,	 164872,  166872,  168872,  G8716xP6, G8716yP4 );   // 166874
	SetCoordinate( 166876,  168876,  170876,  166874,  168874,  170874,	 166872,  168872,  170872,  G8716xP8, G8716yP4 );   // 168874

	// row 87 col 16 grid line 3
	SetCoordinate( 158878,  160878,  162878,  158876,  160876,  162876,  158874,  160874,  162874,  G8716xP0+4, G8716yP6 ); // 160876
	SetCoordinate( 160878,  162878,  164878,  160876,  162876,  164876,	 160874,  162874,  164874,  G8716xP2, G8716yP6 );   // 162876
	SetCoordinate( 162878,  164878,  166878,  162876,  164876,  166876,	 162874,  164874,  166874,  G8716xP4, G8716yP6 );   // 164876
	SetCoordinate( 164878,  166878,  168878,  164876,  166876,  168876,	 164874,  166874,  168874,  G8716xP6, G8716yP6 );   // 166876
	SetCoordinate( 166878,  168878,  170878,  166876,  168876,  170876,	 166874,  168874,  170874,  G8716xP8, G8716yP6 );   // 168876

	// row 87 col 16 grid line 4
	SetCoordinate( 158880,  160880,  162880,  158878,  160878,  162878,  158876,  160876,  162876,  G8716xP0+4, G8716yP8 ); // 160878
	SetCoordinate( 160880,  162880,  164880,  160878,  162878,  164878,	 160876,  162876,  164876,  G8716xP2, G8716yP8 );   // 162878
	SetCoordinate( 162880,  164880,  166880,  162878,  164878,  166878,	 162876,  164876,  166876,  G8716xP4, G8716yP8 );   // 164878
	SetCoordinate( 164880,  166880,  168880,  164878,  166878,  168878,	 164876,  166876,  168876,  G8716xP6, G8716yP8 );   // 166878
	SetCoordinate( 166880,  168880,  170880,  166878,  168878,  170878,	 166876,  168876,  170876,  G8716xP8, G8716yP8 );   // 168878

//-------------------------------------- row 87 col 17 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------

	// row 87 col 17 grid line 0
	SetCoordinate( 168872,  170872,  NO_MGRS, 168870,  170870,  NO_MGRS, 168868,  170868, NO_MGRS, G8717xP0+2, G8717yP0-2 ); // 170870
	// row 87 col 17 grid line 1
	SetCoordinate( 168874,  170874,  NO_MGRS, 168872,  170872,  NO_MGRS, 168870,  170870, NO_MGRS, G8717xP0+2, G8717yP2 ); // 170870
	// row 87 col 17 grid line 2
	SetCoordinate( 168876,  170876,  NO_MGRS, 168874,  170874,  NO_MGRS, 168872,  170872, NO_MGRS, G8717xP0+2, G8717yP4 ); // 170870
	// row 87 col 17 grid line 3
	SetCoordinate( 168878,  170878,  NO_MGRS, 168876,  170876,  NO_MGRS, 168874,  170874, NO_MGRS, G8717xP0+2, G8717yP6 ); // 170870
	// row 87 col 17 grid line 4
	SetCoordinate( 168880,  170880,  NO_MGRS, 168878,  170878,  NO_MGRS, 168876,  170876, NO_MGRS, G8717xP0+2, G8717yP8 ); // 170870

//-------------------------------------- row 88 col 13 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------
	
	// row 88 col 13 grid line 0
	SetCoordinate( NO_MGRS, 130882,  132882,  NO_MGRS, 130880,  132880,  NO_MGRS, 130878,  132878,  G8813xP0, G8813yP0 ); // 130880
	SetCoordinate( 130882,  132882,  134882,  130880,  132880,  134880,	 130878,  132878,  134878,  G8813xP2, G8813yP0 ); // 132880
	SetCoordinate( 132882,  134882,  136882,  132880,  134880,  136880,	 132878,  134878,  136878,  G8813xP4, G8813yP0 ); // 134880
	SetCoordinate( 134882,  136882,  138882,  134880,  136880,  138880,	 134878,  136878,  138878,  G8813xP6, G8813yP0 ); // 136880
	SetCoordinate( 136882,  138882,  140882,  136880,  138880,  140880,	 136878,  138878,  140878,  G8813xP8, G8813yP0 ); // 138880

	// row 88 col 13 grid line 1
	SetCoordinate( NO_MGRS, 130884,  132884,  NO_MGRS, 130882,  132882,  NO_MGRS, 130880,  132880,  G8813xP0, G8813yP2 ); // 130882
	SetCoordinate( 130884,  132884,  134884,  130882,  132882,  134882,	 130880,  132880,  134880,  G8813xP2, G8813yP2 ); // 132882
	SetCoordinate( 132884,  134884,  136884,  132882,  134882,  136882,	 132880,  134880,  136880,  G8813xP4, G8813yP2 ); // 134882
	SetCoordinate( 134884,  136884,  138884,  134882,  136882,  138882,	 134880,  136880,  138880,  G8813xP6, G8813yP2 ); // 136882
	SetCoordinate( 136884,  138884,  140884,  136882,  138882,  140882,	 136880,  138880,  140880,  G8813xP8, G8813yP2 ); // 138882

	// row 88 col 13 grid line 2
	SetCoordinate( NO_MGRS, 130886,  132886,  NO_MGRS, 130884,  132884,  NO_MGRS, 130882,  132882,  G8813xP0, G8813yP4 ); // 130884
	SetCoordinate( 130886,  132886,  134886,  130884,  132884,  134884,	 130882,  132882,  134882,  G8813xP2, G8813yP4 ); // 132884
	SetCoordinate( 132886,  134886,  136886,  132884,  134884,  136884,	 132882,  134882,  136882,  G8813xP4, G8813yP4 ); // 134884
	SetCoordinate( 134886,  136886,  138886,  134884,  136884,  138884,	 134882,  136882,  138882,  G8813xP6, G8813yP4 ); // 136884
	SetCoordinate( 136886,  138886,  140886,  136884,  138884,  140884,	 136882,  138882,  140882,  G8813xP8, G8813yP4 ); // 138884

	// row 88 col 13 grid line 3
	SetCoordinate( NO_MGRS, 130888,  132888,  NO_MGRS, 130886,  132886,  NO_MGRS, 130884,  132884,  G8813xP0-2, G8813yP6 ); // 130886
	SetCoordinate( 130888,  132888,  134888,  130886,  132886,  134886,	 130884,  132884,  134884,  G8813xP2, G8813yP6 ); // 132886
	SetCoordinate( 132888,  134888,  136888,  132886,  134886,  136886,	 132884,  134884,  136884,  G8813xP4, G8813yP6 ); // 134886
	SetCoordinate( 134888,  136888,  138888,  134886,  136886,  138886,	 134884,  136884,  138884,  G8813xP6, G8813yP6 ); // 136886
	SetCoordinate( 136888,  138888,  140888,  136886,  138886,  140886,	 136884,  138884,  140884,  G8813xP8, G8813yP6 ); // 138886

	// row 88 col 13 grid line 4
	SetCoordinate( NO_MGRS, 130890,  132890,  NO_MGRS, 130888,  132888,  NO_MGRS, 130886,  132886,  G8813xP0-2, G8813yP8 ); // 130888
	SetCoordinate( 130890,  132890,  134890,  130888,  132888,  134888,	 130886,  132886,  134886,  G8813xP2, G8813yP8 ); // 132888
	SetCoordinate( 132890,  134890,  136890,  132888,  134888,  136888,	 132886,  134886,  136886,  G8813xP4, G8813yP8 ); // 134888
	SetCoordinate( 134890,  136890,  138890,  134888,  136888,  138888,	 134886,  136886,  138886,  G8813xP6, G8813yP8 ); // 136888
	SetCoordinate( 136890,  138890,  140890,  136888,  138888,  140888,	 136886,  138886,  140886,  G8813xP8, G8813yP8 ); // 138888

//-------------------------------------- row 88 col 14 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------
	
	// row 88 col 14 grid line 0
	SetCoordinate( 138882,  140882,  142882,  138880,  140880,  142880,  138878,  140878,  142878,  G8814xP0, G8814yP0-2 ); // 140880
	SetCoordinate( 140882,  142882,  144882,  140880,  142880,  144880,	 140878,  142878,  144878,  G8814xP2, G8814yP0 ); // 142880
	SetCoordinate( 142882,  144882,  146882,  142880,  144880,  146880,	 142878,  144878,  146878,  G8814xP4, G8814yP0 ); // 144880
	SetCoordinate( 144882,  146882,  148882,  144880,  146880,  148880,	 144878,  146878,  148878,  G8814xP6, G8814yP0-2 ); // 146880
	SetCoordinate( 146882,  148882,  150882,  146880,  148880,  150880,	 146878,  148878,  150878,  G8814xP8, G8814yP0-2 ); // 148880

	// row 88 col 14 grid line 1
	SetCoordinate( 138884,  140884,  142884,  138882,  140882,  142882,  138880,  140880,  142880,  G8814xP0, G8814yP2 ); // 140882
	SetCoordinate( 140884,  142884,  144884,  140882,  142882,  144882,	 140880,  142880,  144880,  G8814xP2, G8814yP2 ); // 142882
	SetCoordinate( 142884,  144884,  146884,  142882,  144882,  146882,	 142880,  144880,  146880,  G8814xP4, G8814yP2 ); // 144882
	SetCoordinate( 144884,  146884,  148884,  144882,  146882,  148882,	 144880,  146880,  148880,  G8814xP6, G8814yP2 ); // 146882
	SetCoordinate( 146884,  148884,  150884,  146882,  148882,  150882,	 146880,  148880,  150880,  G8814xP8, G8814yP2 ); // 148882

	// row 88 col 14 grid line 2
	SetCoordinate( 138886,  140886,  142886,  138884,  140884,  142884,  138882,  140882,  142882,  G8814xP0+2, G8814yP4 ); // 140884
	SetCoordinate( 140886,  142886,  144886,  140884,  142884,  144884,	 140882,  142882,  144882,  G8814xP2, G8814yP4 ); // 142884
	SetCoordinate( 142886,  144886,  146886,  142884,  144884,  146884,	 142882,  144882,  146882,  G8814xP4, G8814yP4 ); // 144884
	SetCoordinate( 144886,  146886,  148886,  144884,  146884,  148884,	 144882,  146882,  148882,  G8814xP6, G8814yP4 ); // 146884
	SetCoordinate( 146886,  148886,  150886,  146884,  148884,  150884,	 146882,  148882,  150882,  G8814xP8, G8814yP4 ); // 148884

	// row 88 col 14 grid line 3
	SetCoordinate( 138888,  140888,  142888,  138886,  140886,  142886,  138884,  140884,  142884,  G8814xP0+2, G8814yP6 ); // 140886
	SetCoordinate( 140888,  142888,  144888,  140886,  142886,  144886,	 140884,  142884,  144884,  G8814xP2, G8814yP6 ); // 142886
	SetCoordinate( 142888,  144888,  146888,  142886,  144886,  146886,	 142884,  144884,  146884,  G8814xP4, G8814yP6 ); // 144886
	SetCoordinate( 144888,  146888,  148888,  144886,  146886,  148886,	 144884,  146884,  148884,  G8814xP6, G8814yP6 ); // 146886
	SetCoordinate( 146888,  148888,  150888,  146886,  148886,  150886,	 146884,  148884,  150884,  G8814xP8, G8814yP6 ); // 148886

	// row 88 col 14 grid line 4
	SetCoordinate( 138890,  140890,  142890,  138888,  140888,  142888,  138886,  140886,  142886,  G8814xP0+2, G8814yP8 ); // 140888
	SetCoordinate( 140890,  142890,  144890,  140888,  142888,  144888,	 140886,  142886,  144886,  G8814xP2, G8814yP8 ); // 142888
	SetCoordinate( 142890,  144890,  146890,  142888,  144888,  146888,	 142886,  144886,  146886,  G8814xP4, G8814yP8 ); // 144888
	SetCoordinate( 144890,  146890,  148890,  144888,  146888,  148888,	 144886,  146886,  148886,  G8814xP6, G8814yP8 ); // 146888
	SetCoordinate( 146890,  148890,  150890,  146888,  148888,  150888,	 146886,  148886,  150886,  G8814xP8, G8814yP8 ); // 148888

//-------------------------------------- row 88 col 15 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------
	
	// row 88 col 15 grid line 0
	SetCoordinate( 148882,  150882,  152882,  148880,  150880,  152880,  148878,  150878,  152878,  G8815xP0, G8815yP0-2 ); // 150880
	SetCoordinate( 150882,  152882,  154882,  150880,  152880,  154880,	 150878,  152878,  154878,  G8815xP2, G8815yP0-2 ); // 152880
	SetCoordinate( 152882,  154882,  156882,  152880,  154880,  156880,	 152878,  154878,  156878,  G8815xP4, G8815yP0-2); // 154880
	SetCoordinate( 154882,  156882,  158882,  154880,  156880,  158880,	 154878,  156878,  158878,  G8815xP6, G8815yP0-2); // 156880
	SetCoordinate( 156882,  158882,  160882,  156880,  158880,  160880,	 156878,  158878,  160878,  G8815xP8, G8815yP0-2 ); // 158880

	// row 88 col 15 grid line 1
	SetCoordinate( 148884,  150884,  152884,  148882,  150882,  152882,  148880,  150880,  152880,  G8815xP0, G8815yP2 ); // 150882
	SetCoordinate( 150884,  152884,  154884,  150882,  152882,  154882,	 150880,  152880,  154880,  G8815xP2, G8815yP2 ); // 152882
	SetCoordinate( 152884,  154884,  156884,  152882,  154882,  156882,	 152880,  154880,  156880,  G8815xP4, G8815yP2 ); // 154882
	SetCoordinate( 154884,  156884,  158884,  154882,  156882,  158882,	 154880,  156880,  158880,  G8815xP6, G8815yP2 ); // 156882
	SetCoordinate( 156884,  158884,  160884,  156882,  158882,  160882,	 156880,  158880,  160880,  G8815xP8, G8815yP2 ); // 158882

	// row 88 col 15 grid line 2
	SetCoordinate( 148886,  150886,  152886,  148884,  150884,  152884,  148882,  150882,  152882,  G8815xP0+2, G8815yP4 ); // 150884
	SetCoordinate( 150886,  152886,  154886,  150884,  152884,  154884,	 150882,  152882,  154882,  G8815xP2, G8815yP4 ); // 152884
	SetCoordinate( 152886,  154886,  156886,  152884,  154884,  156884,	 152882,  154882,  156882,  G8815xP4, G8815yP4 ); // 154884
	SetCoordinate( 154886,  156886,  158886,  154884,  156884,  158884,	 154882,  156882,  158882,  G8815xP6, G8815yP4 ); // 156884
	SetCoordinate( 156886,  158886,  160886,  156884,  158884,  160884,	 156882,  158882,  160882,  G8815xP8, G8815yP4 ); // 158884

	// row 88 col 15 grid line 3
	SetCoordinate( 148888,  150888,  152888,  148886,  150886,  152886,  148884,  150884,  152884,  G8815xP0+2, G8815yP6 ); // 150886
	SetCoordinate( 150888,  152888,  154888,  150886,  152886,  154886,	 150884,  152884,  154884,  G8815xP2, G8815yP6 ); // 152886
	SetCoordinate( 152888,  154888,  156888,  152886,  154886,  156886,	 152884,  154884,  156884,  G8815xP4, G8815yP6 ); // 154886
	SetCoordinate( 154888,  156888,  158888,  154886,  156886,  158886,	 154884,  156884,  158884,  G8815xP6, G8815yP6 ); // 156886
	SetCoordinate( 156888,  158888,  160888,  156886,  158886,  160886,	 156884,  158884,  160884,  G8815xP8, G8815yP6 ); // 158886

	// row 88 col 15 grid line 4
	SetCoordinate( 148890,  150890,  152890,  148888,  150888,  152888,  148886,  150886,  152886,  G8815xP0+2, G8815yP8 ); // 150888
	SetCoordinate( 150890,  152890,  154890,  150888,  152888,  154888,	 150886,  152886,  154886,  G8815xP2, G8815yP8 ); // 152888
	SetCoordinate( 152890,  154890,  156890,  152888,  154888,  156888,	 152886,  154886,  156886,  G8815xP4, G8815yP8 ); // 154888
	SetCoordinate( 154890,  156890,  158890,  154888,  156888,  158888,	 154886,  156886,  158886,  G8815xP6, G8815yP8 ); // 156888
	SetCoordinate( 156890,  158890,  160890,  156888,  158888,  160888,	 156886,  158886,  160886,  G8815xP8, G8815yP8 ); // 158888

//-------------------------------------- row 88 col 16 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------
	
	// row 88 col 16 grid line 0
	SetCoordinate( 158882,  160882,  162882,  158880,  160880,  162880,  158878,  160878,  162878,  G8816xP0+4, G8816yP0-4 ); // 160880
	SetCoordinate( 160882,  162882,  164882,  160880,  162880,  164880,	 160878,  162878,  164878,  G8816xP2, G8816yP0-4 ); // 162880
	SetCoordinate( 162882,  164882,  166882,  162880,  164880,  166880,	 162878,  164878,  166878,  G8816xP4, G8816yP0-4 ); // 164880
	SetCoordinate( 164882,  166882,  168882,  164880,  166880,  168880,	 164878,  166878,  168878,  G8816xP6, G8816yP0-4 ); // 166880
	SetCoordinate( 166882,  168882,  170882,  166880,  168880,  170880,	 166878,  168878,  170878,  G8816xP8, G8816yP0-4 ); // 168880

	// row 88 col 16 grid line 1
	SetCoordinate( 158884,  160884,  162884,  158882,  160882,  162882,  158880,  160880,  162880,  G8816xP0+4, G8816yP2 ); // 160882
	SetCoordinate( 160884,  162884,  164884,  160882,  162882,  164882,	 160880,  162880,  164880,  G8816xP2, G8816yP2 ); // 162882
	SetCoordinate( 162884,  164884,  166884,  162882,  164882,  166882,	 162880,  164880,  166880,  G8816xP4, G8816yP2 ); // 164882
	SetCoordinate( 164884,  166884,  168884,  164882,  166882,  168882,	 164880,  166880,  168880,  G8816xP6, G8816yP2 ); // 166882
	SetCoordinate( 166884,  168884,  170884,  166882,  168882,  170882,	 166880,  168880,  170880,  G8816xP8, G8816yP2 ); // 168882

	// row 88 col 16 grid line 2
	SetCoordinate( 158886,  160886,  162886,  158884,  160884,  162884,  158882,  160882,  162882,  G8816xP0+4, G8816yP4 ); // 160884
	SetCoordinate( 160886,  162886,  164886,  160884,  162884,  164884,	 160882,  162882,  164882,  G8816xP2, G8816yP4 ); // 162884
	SetCoordinate( 162886,  164886,  166886,  162884,  164884,  166884,	 162882,  164882,  166882,  G8816xP4, G8816yP4 ); // 164884
	SetCoordinate( 164886,  166886,  168886,  164884,  166884,  168884,	 164882,  166882,  168882,  G8816xP6, G8816yP4 ); // 166884
	SetCoordinate( 166886,  168886,  170886,  166884,  168884,  170884,	 166882,  168882,  170882,  G8816xP8, G8816yP4 ); // 168884

	// row 88 col 16 grid line 3
	SetCoordinate( 158888,  160888,  162888,  158886,  160886,  162886,  158884,  160884,  162884,  G8816xP0+6, G8816yP6 ); // 160886
	SetCoordinate( 160888,  162888,  164888,  160886,  162886,  164886,	 160884,  162884,  164884,  G8816xP2, G8816yP6 ); // 162886
	SetCoordinate( 162888,  164888,  166888,  162886,  164886,  166886,	 162884,  164884,  166884,  G8816xP4, G8816yP6 ); // 164886
	SetCoordinate( 164888,  166888,  168888,  164886,  166886,  168886,	 164884,  166884,  168884,  G8816xP6, G8816yP6 ); // 166886
	SetCoordinate( 166888,  168888,  170888,  166886,  168886,  170886,	 166884,  168884,  170884,  G8816xP8, G8816yP6 ); // 168886

	// row 88 col 16 grid line 4
	SetCoordinate( 158890,  160890,  162890,  158888,  160888,  162888,  158886,  160886,  162886,  G8816xP0+6, G8816yP8 ); // 160888
	SetCoordinate( 160890,  162890,  164890,  160888,  162888,  164888,	 160886,  162886,  164886,  G8816xP2, G8816yP8 ); // 162888
	SetCoordinate( 162890,  164890,  166890,  162888,  164888,  166888,	 162886,  164886,  166886,  G8816xP4, G8816yP8 ); // 164888
	SetCoordinate( 164890,  166890,  168890,  164888,  166888,  168888,	 164886,  166886,  168886,  G8816xP6, G8816yP8 ); // 166888
	SetCoordinate( 166890,  168890,  170890,  166888,  168888,  170888,	 166886,  168886,  170886,  G8816xP8, G8816yP8 ); // 168888

//-------------------------------------- row 88 col 17 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------
	
	// row 88 col 17 grid line 0
	SetCoordinate( 168882,  170882,  NO_MGRS,  168880, 170880,  NO_MGRS, 168878,  170878,  NO_MGRS,  G8817xP0+2, G8817yP0 - 4); // 170880
	// row 88 col 17 grid line 1
	SetCoordinate( 168884,  170884,  NO_MGRS,  168882, 170882,  NO_MGRS, 168880,  170880,  NO_MGRS,  G8817xP0+2, G8817yP2 ); // 170882
	// row 88 col 17 grid line 2
	SetCoordinate( 168886,  170886,  NO_MGRS,  168884, 170884,  NO_MGRS, 168882,  170882,  NO_MGRS,  G8817xP0+2, G8817yP4 ); // 170884
	// row 88 col 17 grid line 3
	SetCoordinate( 168888,  170888,  NO_MGRS,  168886, 170886,  NO_MGRS, 168884,  170884,  NO_MGRS,  G8817xP0+2, G8817yP6 ); // 170886
	// row 88 col 17 grid line 4
	SetCoordinate( 168890,  170890,  NO_MGRS,  168888, 170888,  NO_MGRS, 168886,  170886,  NO_MGRS,  G8817xP0+2, G8817yP8 ); // 170888

//-------------------------------------- row 89 col 13-17 START ---------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------
	
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  NO_MGRS, 130890,  132890, NO_MGRS, 130888,  132888,  G8913xP0, G8913yP0 ); // 130890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  130890,  132890,  134890, 130888,  132888,  134888,  G8913xP2, G8913yP0 ); // 132890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  132890,  134890,  136890, 132888,  134888,  136888,  G8913xP4, G8913yP0 ); // 134890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  134890,  136890,  138890, 134888,  136888,  138888,  G8913xP6, G8913yP0 ); // 136890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  136890,  138890,  140890, 136888,  138888,  140888,  G8913xP8, G8913yP0 ); // 138890

   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  138890,  140890,  142890, 138888,  140888,  142888,  G8914xP0, G8914yP0 ); // 140890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  140890,  142890,  144890, 140888,  142888,  144888,  G8914xP2, G8914yP0 ); // 142890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  142890,  144890,  146890, 142888,  144888,  146888,  G8914xP4, G8914yP0 ); // 144890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  144890,  146890,  148890, 144888,  146888,  148888,  G8914xP6, G8914yP0 ); // 146890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  146890,  148890,  150890, 146888,  148888,  150888,  G8914xP8, G8914yP0 ); // 148890

   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  148890,  150890,  152890, 148888,  150888,  152888,  G8915xP0, G8915yP0 ); // 140890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  150890,  152890,  154890, 150888,  152888,  154888,  G8915xP2, G8915yP0 ); // 142890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  152890,  154890,  156890, 152888,  154888,  156888,  G8915xP4, G8915yP0 ); // 144890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  154890,  156890,  158890, 154888,  156888,  158888,  G8915xP6, G8915yP0 ); // 146890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  156890,  158890,  160890, 156888,  158888,  160888,  G8915xP8, G8915yP0 ); // 148890

   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  158890,  160890,  162890, 158888,  160888,  162888,  G8916xP0, G8916yP0 ); // 140890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  160890,  162890,  164890, 160888,  162888,  164888,  G8916xP2, G8916yP0 ); // 142890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  162890,  164890,  166890, 162888,  164888,  166888,  G8916xP4, G8916yP0 ); // 144890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  164890,  166890,  168890, 164888,  166888,  168888,  G8916xP6, G8916yP0 ); // 146890
   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  166890,  168890,  170890, 166888,  168888,  170888,  G8916xP8, G8916yP0 ); // 148890

   SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  168890,  170890, NO_MGRS, 168888,  170888,  NO_MGRS, G8917xP0, G8917yP0 ); // 170890
}